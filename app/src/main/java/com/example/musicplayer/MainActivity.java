package com.example.musicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private MediaPlayer player;
    private SeekBar vol;
    private SeekBar sec;
    private int volume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        player = MediaPlayer.create(this,R.raw.yuuyakeyori);


        Button stop = findViewById(R.id.button1);
        Button start = findViewById(R.id.button2);
        Button pause = findViewById(R.id.button3);
        stop.setOnClickListener(this);
        start.setOnClickListener(this);
        pause.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.button1:
                if(player.isPlaying()){
                    player.stop();
                }
                try{
                    player.prepare();
                }catch(Exception e){
                    e.printStackTrace();
                }
                player.release();
                player.seekTo(0);

                break;
            case R.id.button2:
                if(!player.isPlaying()){
                    player.start();
                }
                break;
            case R.id.button3:
                if(player.isPlaying()){
                    player.pause();
                }
                break;

        }
    }
}
